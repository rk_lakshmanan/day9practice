(function () {
    var SListSvc = angular.module("SListSvc", []);

    SListMgr = function ($http) {
        mgr = this;
        mgr.list = [];
        mgr.totalPrice = 0;
        mgr.getList = function () {
            return mgr.list;
        }
        mgr.addToList = function (inputItem, inputQty) {
            console.log('inputItem: ' + inputItem);
            var isFound = false;
            for (var i = 0; i < mgr.list.length; i++) {
                if (mgr.list[i].name == inputItem[1]) {
                    isFound = true;
                    mgr.list[i].qty += inputQty;
                    mgr.list[i].totalItemPrice += inputQty*inputItem[2];
                    mgr.totalPrice += inputQty*inputItem[2];
                }
            }
            if (!isFound) {
                mgr.list.push({
                    name: inputItem[1],
                    qty: inputQty,
                    price: inputItem[2],
                    totalItemPrice: inputQty * inputItem[2]
                })
                mgr.totalPrice+=inputQty*inputItem[2];
            }
            return mgr.totalPrice;
            // console.log("________________"+mgr.totalPrice);
            // console.log(mgr.list)
        }
        mgr.getTotalPrice = ()=>{
            return mgr.totalPrice;
        }
        mgr.sendListToServer = ()=>{
            console.log("server")
            var promise = $http.get("/checkoutCart",{
                params:{
                    list:mgr.list
                }
            }).then((result)=>{
                console.log(result.data);
                return result.data;
            }).catch((error)=>{
                console.log(error.data);
                return error.data;
            });
        }
    }

    SListMgr.$inject = ["$http"];
    SListSvc.service("SListMgr", SListMgr);

})();