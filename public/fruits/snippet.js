var inventory = [
	{ "acorn_squash.png", "Acorn Squash", 1 }, { "apple.png", "Apple", .5 }, 
	{ "bell_pepper.png", "Bell Pepper", .3 }, { "blueberries.png", "Blueberries", .2 }, 
	{ "broccoli.png", "Broccoli", 2 }, { "carrot.png", "Carrot", 1 }, 
	{ "celery.png", "Celery", 1 }, { "chili_pepper.png", "Chili Pepper", .5 }, 
	{ "corn.png", "Corn", 1 }, { "eggplant.png", "Eggplant", .75 }, 
	{ "lettuce.png", "Lettuce", 1 }, { "mushroom.png", "Mushroom", 2 }, 
	{ "onion.png", "Onion", 2 }, { "potato.png", "Potato", 1 }, 
	{ "pumpkin.png", "Pumpkin", 1 }, { "radish.png", "Radish", .5 }, 
	{ "squash.png", "Squash", 2 }, { "strawberry.png", "Strawberry", 1.5 }, 
	{ "sugar_snap.png", "Sugar Snap", 1 }, { "tomato.png", "Tomato", 1 }, 
	{ "zucchini.png", "Zucchini", 1 } 
];
