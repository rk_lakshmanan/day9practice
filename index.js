//Load express
var express = require('express');

//Create app
var app = express();
app.use(express.static(__dirname+"/public"));
app.use("/lib",express.static(__dirname+"/bower_components"));

app.get("/checkoutCart",(req,res)=>{
    // var total = 0;
    // var arr = req.query.list;
    // for(var i = 0;i<arr.length;i++){
    //     console.log(arr)
    // }
    res.status(200);
    res.type("application/json");
    console.log(req.query);
    res.json(req.query)
});

if(process.env.PROD){
    console.log("Running in production");
}else{
    console.log("Testing");
}

// var port = process.argv[2] || process.env.APP_PORT||3000;
var port = process.argv[2] ||3000;
console.log('process.env.APP_PORT: '+process.env.APP_PORT);
console.log(process.env.APP_PORT);

app.listen(port, () => {
    console.log("Application started on port " + port);
})